import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtQuick.Controls.Material 2.2
import QtGraphicalEffects 1.0
import "Methods.js" as JSMethods

SwipeDelegate {
    id: root

    property bool isCurrent: ListView.isCurrentItem
    property bool checkState: model.checkState
    property string itemQuantity: model.itemQuantity
    property string itemName: model.itemName
    property string itemPrice: model.price
    property Window rootObject

    height: itemNameTextField.height + itemNameTextField.topPadding * 2

    onClicked: {
        if (!swipe.complete) {
            itemNameTextField.visible = true;
            itemNameTextField.forceActiveFocus();
        }
    }

    Item {
        anchors.fill: contentItem

        Item {
            id: filler

            z: -2
            width: checkStateCheckBox.width
            height: checkStateCheckBox.height
        }

        Text {
            id: itemNameMask

            anchors.left: filler.right
            width: itemNameTextField.width - itemPriceMask.width
            height: itemNameTextField.height
            verticalAlignment: Text.AlignVCenter
            wrapMode: itemNameTextField.wrapMode
            text: itemName
            font.pixelSize: itemNameTextField.font.pixelSize
            visible: !itemNameTextField.visible
            color: checkStateCheckBox.checked ? "#757575" : "#212121"
        }

        Text {
            id: itemPriceMask

            anchors.right: parent.right
            verticalAlignment: Text.AlignVCenter
            visible: !itemNameTextField.visible
            text: itemPrice
            font.pixelSize: itemNameMask.font.pixelSize
            height: itemNameMask.height
        }
    }

    contentItem: Row {
        CheckBox {
            id: checkStateCheckBox

            property string dialogTitle: qsTr("Enter item price")
            property string dialogPlaceholder: qsTr("item price")
            property string dialogStr: "InputDialog{
                        value: model.price;
                        title: \"%1\";
                        placeholderText: \"%2\";
                        onPlusButtonClicked: {
                            shoppingListModel.push(index, value);
                            console.log(value);
                        }
                        onAccepted: {
                            if (value) {
                                model.price = value;
                            }
                            model.checkState = checkStateCheckBox.checked;
                        }
                        onRejected: checkStateCheckBox.checked = false
                    }".arg(dialogTitle).arg(dialogPlaceholder)

            checked: root.checkState

            onClicked: {
                root.ListView.view.currentIndex = index
                if (checked && !model.price) {
                    JSMethods.createDialog(dialogStr, rootObject, "dynamicDialog");
                } else {
                    model.checkState = checked
                }
            }
        }

        TextField {
            id: itemNameTextField

            width: root.width -
                   itemPriceMask.width -
                   checkStateCheckBox.width -
                   checkStateCheckBox.leftPadding * 2
            wrapMode: Text.WordWrap
            text: itemName
            visible: false
            selectByMouse: true

            onEditingFinished: {
                model.itemName = text
                visible = false
            }
            onVisibleChanged: {
                if (visible) {
                    root.ListView.view.currentIndex = index
                }
            }
        }
    }

    ListView.onRemove: SequentialAnimation {
        PropertyAction {
            target: root
            property: "ListView.delayRemove"
            value: true
        }
        NumberAnimation {
            target: root
            property: "height"
            to: 0
            easing.type: Easing.InOutQuad
        }
        PropertyAction {
            target: root
            property: "ListView.delayRemove"
            value: false
        }
    }

    swipe.right: Row {
        height: root.height
        spacing: height * 0.1
        rightPadding: spacing
        leftPadding: spacing
        anchors.right: parent.right

        Label {
            id: quantityLabel

            property string dialogTitle: qsTr("Change item quantity")
            property string dialogPlaceholder: qsTr("new quantity")
            property string dialogStr: "InputDialog {
                                value: model.itemQuantity;
                                title: \"%1\";
                                placeholderText: \"%2\";
                                pushPriceButton: false;
                                onAccepted: model.itemQuantity = value }".arg(dialogTitle).
                                                                            arg(dialogPlaceholder)

            text: "x%1".arg(itemQuantity)
            height: parent.height
            verticalAlignment: Text.AlignVCenter
            color: "gray"

            MouseArea {
                id: newQuantityTappableArea

                anchors.fill: parent

                onClicked: {
                    JSMethods.createDialog(quantityLabel.dialogStr, rootObject, "dynamicDialog");
                    swipe.close();
                }
            }
        }

        Item {
            id: newPriceButton

            property string dialogTitle: qsTr("Change item price")
            property string dialogPlaceholder: qsTr("new price")
            property string dialogStr: "InputDialog{value: model.price;
                            title: \"%1\";
                            placeholderText: \"%2\";
                            onPlusButtonClicked: {
                                shoppingListModel.push(index, value);
                                console.log(value);
                            }
                            onAccepted: {
                                if (value) {
                                    model.price=value;
                                }
                            }
                        }".arg(dialogTitle).arg(dialogPlaceholder)

            anchors.verticalCenter: parent.verticalCenter
            height: checkStateCheckBox.height//parent.height// * 0.8
            width: height

            MouseArea {
                id: newPriceTappableArea

                anchors.fill: newPriceButton

                onClicked: {
                    JSMethods.createDialog(newPriceButton.dialogStr, rootObject, "dynamicDialog");
                    swipe.close();
                }
            }
            Image {
                id: priceTag

                source: "images/price-tag.png"
                width: parent.width * 0.75
                height: width
                anchors.centerIn: parent
                mipmap: true
            }
            ColorOverlay {
                anchors.fill: priceTag
                source: priceTag
                color: newPriceTappableArea.pressed ? "#303F9F" : "transparent"
            }
        }
        Item {
            id: deleteItemButton

            anchors.verticalCenter: parent.verticalCenter
            height: checkStateCheckBox.height//parent.height// * 0.8
            width: height

            MouseArea {
                id: deleteTappableArea

                anchors.fill: deleteItemButton

                onClicked: root.ListView.view.model.removeItem(index)
            }
            Image {
                id: garbage

                source: "images/garbage.png"
                width: parent.width * 0.8
                height: parent.height * 0.8
                anchors.centerIn: parent
                mipmap: true
            }
            ColorOverlay {
                anchors.fill: garbage
                source: garbage
                color: deleteTappableArea.pressed ? "#D32F2F" : "transparent"
            }
        }
    }

    swipe.onOpened: ListView.view.currentIndex = index
    onCheckStateChanged: ListView.view.currentIndex = index
    onIsCurrentChanged: {
        if (!isCurrent) {
            swipe.close();
        }
    }
}
