import QtQuick 2.9
import QtQuick.Controls 2.3

MenuBarItem {
    height: 48
    width: parent.width
    font.pixelSize: 14
    spacing: 16
    icon.color: "#9E9E9E"
}
