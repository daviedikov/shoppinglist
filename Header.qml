import QtQuick 2.0
import QtQuick.Controls 2.2
//import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3

ToolBar {
    id: root

    property string text

    signal tabButtonClicked()

    RowLayout {
        anchors.fill: parent
        ToolButton {
            icon.name: "drawer-icon"
            icon.source: "images/drawer-icon.png"
            onClicked: root.tabButtonClicked()
        }
        Label {
            text: root.text
            font.pixelSize: 20
            horizontalAlignment: Qt.AlignLeft
            verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: true
        }
    }
}
