import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3


Dialog {
    id: root

    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    width: parent.width * 0.9
    modal: true
    Material.elevation: 16
    title: qsTr("About")
    contentItem: Label {
            Material.theme: Material.System
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
            Layout.topMargin: 14
            text: "Shopping List app v1.0.1.<br>
                    <br>
                    %1 <a href='https://www.flaticon.com/authors/gregor-cresnar'>Gregor Cresnar</a>,
                    <a href='https://www.flaticon.com/authors/smashicons'>Smashicons</a>
                    %2 <a href='https://www.flaticon.com'>www.flaticons.com</a><br>
                    <a href='https://www.freepik.com/free-photos-vectors/background'>
                    %3 Freepik</a><br>
                    <br>
                    %4 <br>
                    <br>
                    %5".
            arg(qsTr("Icons made by")).
            arg(qsTr("from")).
            arg(qsTr("Background vector created by")).
            arg(qsTr("Author: Dmytro Aviedikov")).
            arg(qsTr("Special thanks to Yevheniia Taltanova."))

            onLinkActivated: Qt.openUrlExternally(link)


    }
    standardButtons: Dialog.NoButton
}
