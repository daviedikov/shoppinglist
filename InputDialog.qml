import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Window 2.2
import QtQml 2.2
import QtQuick.Layouts 1.3

Dialog {
    id: root

    property string value
    property string placeholderText
    property string role: "price"
    property bool pushPriceButton: true

    signal plusButtonClicked()

    x: (/*Screen.desktopAvailableWidth*/parent.width - width) / 2
    //it's a bit tricky, but Qt.inputMethod.keyboardRectangle.height doesn't return actual
    //height of android keyboard :(
    y: Qt.inputMethod.visible ? (/*Screen.desktopAvailableHeight*/parent.height / 2 - height) / 2 :
                                (/*Screen.desktopAvailableHeight*/parent.height - height) / 2
    focus: true
    modal: true
    closePolicy: Popup.CloseOnEscape
    width: parent.width * 0.9
    standardButtons: Dialog.Cancel | Dialog.Ok
    Material.elevation: 24

    contentItem: RowLayout {
        TextField {
            id: input

            width: parent.width
            placeholderText: root.placeholderText
            inputMethodHints: role == "price" ? Qt.ImhDigitsOnly : Qt.ImhNone

            Layout.fillWidth: true
            focus: true
            text: {
                if (root.role == "price") {
                    value == "0" ? "" : value
                } else {
                    value
                }
            }
            validator: RegExpValidator {
                regExp: root.role == "price" ? /\d{1,4}([.]\d{1,2})?$/ : /.*/
            }

            onAccepted: {
                root.accept();
                clear();
            }
        }
        RoundButton {
            id: plus

            text: "+"
            font.bold: true
            flat: true
            visible: root.pushPriceButton
            Material.foreground: Material.Amber

            onClicked: {
                if (input.text) {
                    value = input.text
                    root.plusButtonClicked()
                    input.clear()
                    value = ""
                    input.focus = true
                }
            }
        }
    }

    onAccepted: {
        if (input.text) {
            value = input.text
        }
        root.destroy();
    }
    onRejected: root.destroy();
}
