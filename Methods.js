function createDialog(str, parent, filepath) {
    try {
        var dialog =
                Qt.createQmlObject(str, parent, filepath)
        dialog.open();
    }
    catch (error) {
        console.error(error);
        parent.close();
    }
}


