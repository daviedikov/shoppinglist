import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.2

Drawer {
    id: root

    property int imageWidth: root.width
    property int imageHeight

    signal clearListClicked()
    signal aboutClicked()

    height: parent.height
    Material.elevation: 16
    Material.theme: Material.System

    contentItem: Item {
        Image {
            id: drawerBackground

            source: "images/drawer-background.png"
            fillMode: Image.PreserveAspectCrop
            width: root.imageWidth
            height: imageHeight
        }

        DrawerButton {
            id: clearListButton

            anchors.top: drawerBackground.bottom
            icon.name: "clear-list"
            icon.source: "images/clear-list.png"
            text: qsTr("Clear list")

            onClicked: root.clearListClicked()
        }

        MenuSeparator {
            id: separator

            anchors.top: clearListButton.bottom
            width: root.width
            topPadding: 8
            bottomPadding: topPadding
        }

        DrawerButton {
            id: aboutButton

            anchors.top: separator.bottom
            icon.name: "info"
            icon.source: "images/info.png"
            text: qsTr("About...")

            onClicked: root.aboutClicked()
        }
    }
}
