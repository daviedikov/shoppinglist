QT += quick
QT += quickcontrols2
CONFIG += c++11
CONFIG += console

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS QT_MESSAGELOGCONTEXT
PRECOMPILED_HEADER =

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    shoppinglistmodel.cpp \
    loggingcategories.cpp \
    logger.cpp \
    purchase.cpp


RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    shoppinglistmodel.h \
    loggingcategories.h \
    logger.h \
    purchase.h

INCLUDEPATH += C:\boost_1_66_0
TR_EXCLUDE = C:\boost_1_66_0\*
DISTFILES += \
    sh_lst_ru.ts \
    sh_lst_ua.ts \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    android/assets/translations/sh_lst_ru.qm

TRANSLATIONS = sh_lst_ru.ts \
    sh_lst_ua.ts

android {
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    TRDESTDIR = $$system_path($$PWD/android/assets/translations)
    QMAKE_POST_LINK = $$QMAKE_COPY $$shell_path($$[QT_INSTALL_TRANSLATIONS]/qt*_ru.qm) $$TRDESTDIR
}
