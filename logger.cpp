#include "logger.h"
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
Logger::Logger()
{
    if (!QDir(QString("Log")).exists()) {
        QDir().mkdir(QString("Log"));
    }
    deleteOldFiles();
    logFile_.reset(new QFile(QString("Log/%1_logfile.log").arg(QDate::currentDate().toString("yy-MM-dd"))));
    logFile_.data()->open(QFile::Append | QFile::Text);

}

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Logger &logger = Logger::init();
    QTextStream out(logger.getLogFile().data());
    out << QDateTime::currentDateTime().toString("yy-MM-dd hh:mm:ss.zzz ");
    switch (type) {
    case QtInfoMsg:
        out << "[INF] ";
        break;
    case QtDebugMsg:
        out << "[DBG] ";
        break;
    case QtWarningMsg:
        out << "[WARN] ";
        break;
    case QtCriticalMsg:
        out << "[CRIT] ";
        break;
    case QtFatalMsg:
        out << "[FATAL] ";
        break;
        //no default
    }
    out << context.category << ": " << msg << endl;
    out.flush();
}

Logger& Logger::init()
{
    static Logger singleLogger;
    return singleLogger;
}
//not tested yet
void Logger::deleteOldFiles()
{
    QDir log("Log");
    auto entryList = log.entryList(QStringList() << "*.log", QDir::Files, QDir::Time);
    auto count = entryList.count();
    if (count <= 10)
        return;
    for (auto i = count - 1; i < count; ++i) {
        log.remove(entryList.at(i));
    }
}
