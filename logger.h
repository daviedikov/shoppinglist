#ifndef LOGGER_H
#define LOGGER_H

#include <QScopedPointer>
class QFile;

class Logger //singleton
{
private:
    QScopedPointer<QFile> logFile_;

    Logger();
    Logger(const Logger &right) = delete;
    Logger &operator=(const Logger &right) = delete;
    static void deleteOldFiles();
public:
    static Logger &init();
    friend void
        messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    QScopedPointer<QFile> &getLogFile() { return logFile_; }
};

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

#endif // LOGGER_H
