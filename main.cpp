#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQuickStyle>
#include <QQmlContext>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>
#include <QDebug>
#include "logger.h"
#include "loggingcategories.h"
#include "shoppinglistmodel.h"

int main(int argc, char *argv[])
{
    qputenv("QT_QPA_NO_TEXT_HANDLES", "1");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL);
    QGuiApplication app(argc, argv);
    QTranslator translator;
    QTranslator baseTranslator;

    QQuickStyle::setStyle("Material");
    qInstallMessageHandler(messageHandler);
    qmlRegisterType<ShoppingListModel>("ShoppingListModel", 1, 0, "ShoppingListModel");

    QQmlApplicationEngine engine;
    translator.load(QString("sh_lst_") + QLocale::system().name(), QString("assets:/translations"));
    baseTranslator.load(QString("qtbase_") + QLocale::system().name(), QString("assets:/translations"));
    app.installTranslator(&translator);
    app.installTranslator(&baseTranslator);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;
    return app.exec();
}
