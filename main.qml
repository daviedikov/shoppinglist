import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import ShoppingListModel 1.0
import QtQuick.Controls.Material 2.2
import "Methods.js" as JSMethods

ApplicationWindow {
    id: mainWindow

    property int appState: Qt.application.state

    Material.theme: Material.System
    Material.primary: Material.Teal
    Material.accent: Material.Amber

    visible: true
    width:  360 //Screen.desktopAvailableWidth
    height: 480 //Screen.desktopAvailableHeight
    title: qsTr("Shopping List")
    header: Header {
        Material.elevation: 4
        height: 56
        text: qsTr("Total price: %1").arg(shoppingListModel.totalPrice)
        onTabButtonClicked: drawer.open()
    }

    ShoppingListModel {
        id: shoppingListModel
    }

    Info {
        id: info

        visible: false
    }

    NavDrawer {
        id: drawer

        width: mainWindow.width - mainWindow.header.height
        imageHeight: mainWindow.header.height * 2.5

        Material.elevation: 16

        onClearListClicked: {
            shoppingListModel.clear();
            close();
        }
        onAboutClicked: {
            close();
            info.visible = true
        }
    }

    ListView {
        id: mainView

        model: shoppingListModel
        anchors.fill: parent
        spacing: 1

        delegate: DelegateBody {
            id: item
            width: mainView.width
            rootObject: mainWindow
        }
    }

    RoundButton {
        id: newItemButton

        property string dialogTitle: qsTr("Add new item")
        property string dialogPlaceholder: qsTr("item name")
        property string dialogString: "InputDialog {
                value: \"\";
                title: \"%1\";
                placeholderText: \"%2\";
                role: \"name\"
                pushPriceButton: false
                onAccepted: {
                    if (value) {
                        shoppingListModel.addItem(value);
                        mainView.focus = true
                    }
                }
            }".arg(dialogTitle).arg(dialogPlaceholder)

        x: mainWindow.width - width - 16
        y: mainWindow.height - mainWindow.header.height - height - 16
        Material.elevation: 6
        Material.background: Material.Amber
        Material.foreground: "white"
        text: "+"
        width: 56
        height: width
        visible: mainView.currentIndex == -1  || !mainView.currentItem.swipe.complete

        onClicked:  {
            mainView.currentIndex = -1
            JSMethods.createDialog(dialogString, mainWindow, "newItemDialog");
        }
    }

    onAppStateChanged: {
        if (appState === Qt.ApplicationSuspended) {
            shoppingListModel.writeList();
        }
    }
}
