#include <QString>
#include <QDataStream>
#include <QDebug>
#include "purchase.h"

using boost::multiprecision::cpp_dec_float_50;

Purchase::Purchase()
{
    content_.first = cpp_dec_float_50(1.0);
}

Purchase::Purchase(const QString &name)
{
    content_ = QPair<cpp_dec_float_50, QPair<QString, cpp_dec_float_50>>
            (cpp_dec_float_50(1.0), QPair<QString, cpp_dec_float_50>(name, cpp_dec_float_50(0.0)));
}

const QString &Purchase::getName() const
{
    return content_.second.first;
}

const cpp_dec_float_50 &Purchase::getPrice() const
{
    return content_.second.second;
}

const cpp_dec_float_50 &Purchase::getQuantity() const
{
    return content_.first;
}

void Purchase::setName(const QString &name)
{
    content_.second.first = name;
}

void Purchase::setPrice(const cpp_dec_float_50 &price)
{
    Q_ASSERT(price >= cpp_dec_float_50(0.0));
    content_.second.second = price;
}

void Purchase::setQuantity(const cpp_dec_float_50 &quantity)
{
    Q_ASSERT(quantity >= cpp_dec_float_50(0.0));
    content_.first = quantity;
}

QDataStream &operator<<(QDataStream &out, const cpp_dec_float_50 &value)
{
    size_t len = sizeof(value);
    char *raw = (char*)(&value);
    int bytesWritten = out.writeRawData(raw, len);
    Q_ASSERT(bytesWritten != -1);
    return out;
}

QDataStream &operator>>(QDataStream &in, cpp_dec_float_50 &value)
{
    size_t len = sizeof(value);
    char *raw = (char*)(&value);
    int bytesRead = in.readRawData(raw, len);
    Q_ASSERT(bytesRead != -1);
    return in;
}

QDataStream &operator<<(QDataStream &out, const Purchase &purchase)
{
    out << purchase.content_.first
        << purchase.content_.second.first
        << purchase.content_.second.second;
    return out;
}

QDataStream &operator>>(QDataStream &in, Purchase &purchase)
{
    in >> purchase.content_.first
        >> purchase.content_.second.first
        >> purchase.content_.second.second;
    return in;
}
