#ifndef PURCHASE_H
#define PURCHASE_H

#include <QPair>
#include <boost/multiprecision/cpp_dec_float.hpp>

class Purchase
{
private:
    QPair<boost::multiprecision::cpp_dec_float_50,
        QPair<QString, boost::multiprecision::cpp_dec_float_50>> content_;
public:
    Purchase();
    Purchase(const QString &name);

    const QString &getName() const;
    const boost::multiprecision::cpp_dec_float_50 &getPrice() const;
    const boost::multiprecision::cpp_dec_float_50 &getQuantity() const;

    void setName(const QString &name);
    void setPrice(const boost::multiprecision::cpp_dec_float_50 &price);
    void setQuantity(const boost::multiprecision::cpp_dec_float_50 &quantity);

    friend QDataStream &operator<<(QDataStream &out, const Purchase &purchase);
    friend QDataStream &operator>>(QDataStream &in, Purchase &purchase);
};

QDataStream &operator<<(QDataStream &out, const boost::multiprecision::cpp_dec_float_50 &value);
QDataStream &operator>>(QDataStream &in, boost::multiprecision::cpp_dec_float_50 &value);

QDataStream &operator<<(QDataStream &out, const Purchase &purchase);
QDataStream &operator>>(QDataStream &in, Purchase &purchase);

#endif // PURCHASE_H
