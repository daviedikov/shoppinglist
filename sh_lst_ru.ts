<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DelegateBody</name>
    <message>
        <location filename="DelegateBody.qml" line="60"/>
        <source>Enter item price</source>
        <translation>Введите цену покупки</translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="61"/>
        <source>item price</source>
        <translation>цена покупки</translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="129"/>
        <source>Change item quantity</source>
        <translation>Изменить количество</translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="130"/>
        <source>new quantity</source>
        <translation>новое количество</translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="152"/>
        <source>Change item price</source>
        <translation>Изменить цену покупки</translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="153"/>
        <source>new price</source>
        <translation>новая цена</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="Info.qml" line="15"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="Info.qml" line="31"/>
        <source>Icons made by</source>
        <translation>Используются иконки, созданные</translation>
    </message>
    <message>
        <location filename="Info.qml" line="32"/>
        <source>from</source>
        <translation>с сайта</translation>
    </message>
    <message>
        <location filename="Info.qml" line="33"/>
        <source>Background vector created by</source>
        <translation>Векторный фон создан</translation>
    </message>
    <message>
        <location filename="Info.qml" line="34"/>
        <source>Author: Dmytro Aviedikov</source>
        <translation>Автор: Дмитрий Аведиков</translation>
    </message>
    <message>
        <location filename="Info.qml" line="35"/>
        <source>Special thanks to Yevheniia Taltanova.</source>
        <translation>Особая благодарность Евгении Талтановой.</translation>
    </message>
</context>
<context>
    <name>NavDrawer</name>
    <message>
        <location filename="NavDrawer.qml" line="28"/>
        <source>Clear list</source>
        <translation>Очистить список</translation>
    </message>
    <message>
        <location filename="NavDrawer.qml" line="45"/>
        <source>About...</source>
        <translation>О программе...</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="19"/>
        <source>Shopping List</source>
        <translation>Список покупок</translation>
    </message>
    <message>
        <location filename="main.qml" line="23"/>
        <source>Total price: %1</source>
        <translation>Общая стоимость: %1</translation>
    </message>
    <message>
        <location filename="main.qml" line="80"/>
        <source>Add new item</source>
        <oldsource>&quot;Add new item&quot;</oldsource>
        <translation>Добавить новую покупку</translation>
    </message>
    <message>
        <location filename="main.qml" line="81"/>
        <source>item name</source>
        <oldsource>&quot;item name&quot;</oldsource>
        <translation>название покупки</translation>
    </message>
</context>
</TS>
