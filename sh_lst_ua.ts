<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_UA">
<context>
    <name>DelegateBody</name>
    <message>
        <location filename="DelegateBody.qml" line="60"/>
        <source>Enter item price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="61"/>
        <source>item price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="129"/>
        <source>Change item quantity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="130"/>
        <source>new quantity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="152"/>
        <source>Change item price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="DelegateBody.qml" line="153"/>
        <source>new price</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="Info.qml" line="15"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="Info.qml" line="31"/>
        <source>Icons made by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Info.qml" line="32"/>
        <source>from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Info.qml" line="33"/>
        <source>Background vector created by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Info.qml" line="34"/>
        <source>Author: Dmytro Aviedikov</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Info.qml" line="35"/>
        <source>Special thanks to Yevheniia Taltanova.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavDrawer</name>
    <message>
        <location filename="NavDrawer.qml" line="28"/>
        <source>Clear list</source>
        <translatorcomment>Очистить список</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="NavDrawer.qml" line="45"/>
        <source>About...</source>
        <translation>О программе...</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="19"/>
        <source>Shopping List</source>
        <translation>Списко покупок</translation>
    </message>
    <message>
        <location filename="main.qml" line="23"/>
        <source>Total price: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="80"/>
        <source>Add new item</source>
        <oldsource>&quot;Add new item&quot;</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="81"/>
        <source>item name</source>
        <oldsource>&quot;item name&quot;</oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
