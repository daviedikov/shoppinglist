#include "shoppinglistmodel.h"
#include <QList>
#include <QPair>
#include <QString>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include <QDir>
//#include <boost/multiprecision/cpp_dec_float.hpp>
#include "loggingcategories.h"
#include "purchase.h"

using boost::multiprecision::cpp_dec_float_50;

ShoppingListModel::ShoppingListModel(QObject *parent): QAbstractListModel(parent)
{
    qInfo(logInfo()) << "Session starts";
    initList();
    qInfo(logInfo()) << "model created";
}

ShoppingListModel::~ShoppingListModel()
{
    writeList();
    qInfo(logInfo()) << "Session ends" << endl;
}

int ShoppingListModel::rowCount(const QModelIndex &parent) const
{
    Q_ASSERT(!parent.isValid());
    return data_.size();
}

QVariant ShoppingListModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(index.isValid() && roleNames().contains(role));
    switch (role) {
    case CheckStateRole:
        return data_.at(index.row()).first;
    case ItemQuantityRole:
        return data_.at(index.row()).second.getQuantity().convert_to<double>();
    case ItemNameRole:
        return data_.at(index.row()).second.getName();
    case PriceRole:
        return data_.at(index.row()).second.getPrice().convert_to<double>();
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> ShoppingListModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractListModel::roleNames();
    roles[CheckStateRole] = "checkState";
    roles[ItemQuantityRole] = "itemQuantity";
    roles[ItemNameRole] = "itemName";
    roles[PriceRole] = "price";

    return roles;
}

bool ShoppingListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_ASSERT(index.isValid() && roleNames().contains(role));

    switch (role) {
    case CheckStateRole: {
//        auto price = data_.at(index.row()).second.getPrice();
//        auto quantity = data_.at(index.row()).second.getQuantity();
        auto cost = itemCost(index.row());
        qInfo(logInfo()) << "checkState changed";
        value.toBool() ? changeTotal(cpp_dec_float_50(), cost) :
                         changeTotal(cost, cpp_dec_float_50());
        data_[index.row()].first = value.toBool();
        break;
    }
    case ItemQuantityRole: {
        qInfo(logInfo()) << "itemQuantity changed";
        bool success;
        qreal dQuantity = value.toDouble(&success);
        Q_ASSERT(success);
        if (data_.at(index.row()).first) {
            auto price = data_.at(index.row()).second.getPrice();
            auto oldQuantity = data_.at(index.row()).second.getQuantity();
            changeTotal(price * oldQuantity, price * cpp_dec_float_50(dQuantity));
        }
        data_[index.row()].second.setQuantity(cpp_dec_float_50(dQuantity));
        break;
    }
    case ItemNameRole:
        qInfo(logInfo()) << "itemName changed";
        data_[index.row()].second.setName(value.toString());
        break;
    case PriceRole: {
        qInfo(logInfo()) << "price changed";
        bool success;
        qreal dPrice = value.toDouble(&success);
        Q_ASSERT(success);

        if (data_.at(index.row()).first) {
            auto cost = itemCost(index.row());
            changeTotal(cost, cpp_dec_float_50(dPrice) * data_.at(index.row()).second.getQuantity());
        }
        data_[index.row()].second.setPrice(cpp_dec_float_50(dPrice));
        break;
    }
    default:
        return false;
    }
    emit dataChanged(index, index, QVector<int>() << role);
    return true;
}

void ShoppingListModel::addItem(const QVariant &itemName)
{
    qInfo(logInfo()) << "addItem: begin";
    beginInsertRows(QModelIndex(), data_.size(), data_.size());
    data_.append(QPair<bool, Purchase>(false, Purchase(itemName.toString())));
    endInsertRows();
    qInfo(logInfo()) << "addItem: success";
}

void ShoppingListModel::removeItem(const int row)
{
    qInfo(logInfo()) << "removeItem: begin";
    Q_ASSERT(index(row).isValid());
    beginRemoveRows(QModelIndex(), row, row);
    if (data_.at(row).first) {
        auto oldPrice = data_.at(row).second.getPrice();
        auto quantity = data_.at(row).second.getQuantity();
        changeTotal(oldPrice * quantity, cpp_dec_float_50(0.0));
    }
    data_.erase(data_.begin() + row);
    endRemoveRows();
    qInfo(logInfo()) << "removeItem: success";
}
void ShoppingListModel::clear()
{
    qInfo(logInfo()) << "clear: begin";
    if (data_.isEmpty()) {
        qInfo(logInfo()) << "clear: model is empty, leaving func";
        return;
    }
    beginRemoveRows(QModelIndex(), 0, data_.size() - 1);
        data_.clear();
        changeTotal(totalPrice_, cpp_dec_float_50());
    endRemoveRows();
    qInfo(logInfo()) << "clear: success";
}

void ShoppingListModel::changeTotal(const cpp_dec_float_50 &oldPrice,
                                    const cpp_dec_float_50 &newPrice)
{
    qInfo(logInfo()) << "changeTotal: begin";
    qInfo(logInfo()) <<
        QString("changeTotal(%1, %2)").arg(QString::number(oldPrice.convert_to<double>())).
                                           arg(QString::number(newPrice.convert_to<double>()));
    totalPrice_ += (newPrice - oldPrice);
    cpp_dec_float_50 zero = cpp_dec_float_50(0.0);
    Q_ASSERT(totalPrice_ >= zero);
    emit totalChanged(totalPrice_.convert_to<double>());
    qInfo(logInfo()) << "changeTotal: success";
}

void ShoppingListModel::initList()
{
    qInfo(logInfo()) << "initList: begin";
    QFile file("data.shlst");
    if (file.open(QIODevice::ReadOnly)) {
        QDataStream in(&file);
        //TODO: use something else instead of exception if reading from file failed
        try {
            in >> data_ >> totalPrice_;
        } catch (...) {
            file.remove();
        }
        qInfo(logInfo()) << "initList: data.shlst opened";
        return;
    }
    qInfo(logInfo()) << "initList: data.shlst doesn't exist, new file will be created";
}

void ShoppingListModel::writeList() const
{
    qInfo(logInfo()) << "writeList: begin";
    QFile file("data.shlst");
    if (file.open(QIODevice::WriteOnly)) {
        QDataStream out(&file);
        out << data_ << totalPrice_;
    }
    qInfo(logInfo()) << "writeList: success";
}

void ShoppingListModel::push(const int row, const QVariant &value)
{
    qInfo(logInfo()) << "push: begin";
    auto modelIndex = index(row);
    auto oldPrice = cpp_dec_float_50(data(modelIndex, PriceRole).toDouble());
    bool success;
    auto decValue = cpp_dec_float_50(value.toDouble(&success));
    Q_ASSERT(success);
    bool set = setData(modelIndex, QVariant((oldPrice + decValue).convert_to<double>()), PriceRole);
    qInfo(logInfo()) << QString("row: %1"
                                "indexIsValid: %2"
                                "oldPrice: %3"
                                "decValue: %4"
                                "set: %5").arg(row)
                        .arg(modelIndex.isValid())
                        .arg(oldPrice.convert_to<double>())
                        .arg(decValue.convert_to<double>())
                        .arg(set);
    qInfo(logInfo()) << "push: end";
}

Qt::ItemFlags ShoppingListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}

qreal ShoppingListModel::total() const
{
    return totalPrice_.convert_to<double>();
}

cpp_dec_float_50 ShoppingListModel::itemCost(const int row) const
{
    return data_.at(row).second.getPrice() * data_.at(row).second.getQuantity();
}
