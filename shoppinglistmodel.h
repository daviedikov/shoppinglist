#ifndef SHOPPINGLISTMODEL_H
#define SHOPPINGLISTMODEL_H

#include <QAbstractListModel>
#include <boost/multiprecision/cpp_dec_float.hpp>

class Purchase;

class ShoppingListModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(qreal totalPrice READ total NOTIFY totalChanged) //here
public:
    enum Roles {
        CheckStateRole = Qt::UserRole + 1,
        ItemQuantityRole,
        ItemNameRole,
        PriceRole
    };

    ShoppingListModel(QObject *parent = Q_NULLPTR);
    ~ShoppingListModel();

    virtual int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    virtual QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    virtual QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) Q_DECL_OVERRIDE;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    qreal total() const; //adapter function to pass cpp_dec_float50 value to qml layer

    Q_INVOKABLE void addItem(const QVariant &itemName);
    Q_INVOKABLE void removeItem(const int row);
    Q_INVOKABLE void clear();
    Q_INVOKABLE void writeList() const;
    Q_INVOKABLE void push(const int row, const QVariant &value); //dirty hack, has to be rewrited
    //summarizes the value passed to it with price of the item in passed row
private:
    QList<QPair<bool, Purchase>> data_;
    boost::multiprecision::cpp_dec_float_50 totalPrice_;

    boost::multiprecision::cpp_dec_float_50 itemCost(int row) const;

    void changeTotal(const boost::multiprecision::cpp_dec_float_50 &oldPrice,
                     const boost::multiprecision::cpp_dec_float_50 &newPrice);
    void initList();


signals:
    void totalChanged(qreal total);
};

#endif // SHOPPINGLISTMODEL_H
